import express from "express";
import { authenticate } from "./../auth/verifyToken.js";
import {
  getCheckoutSession,
  getAllDoctor,
} from "../Controllers/bookingController.js";

const router = express.Router();

router.post("/checkout-session/:doctorId", authenticate, getCheckoutSession);
router.get("/:id", getAllDoctor);

export default router;
