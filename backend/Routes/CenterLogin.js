import express from "express";
import {LoginDiagnostic} from "../Controllers/DiagnosticCenterLoginController.js";
const router = express.Router();

router.get("/Centerlogin", LoginDiagnostic);

export default router;