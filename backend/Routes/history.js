import express from "express";
import { authenticate } from "./../auth/verifyToken.js";
// import { getCheckoutSession } from "../Controllers/bookingController.js";
import {
  createHistory,
  getAllHistory,
  getHistoryById,
} from "../Controllers/historyController.js";

const router = express.Router();

router.post("/patient/:doctorId", authenticate, createHistory);
router.get("/:id", getAllHistory);
router.get("/:id", authenticate, getHistoryById);

export default router;
