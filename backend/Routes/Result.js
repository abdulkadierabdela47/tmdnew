import { authenticate, restrict } from "./../auth/verifyToken.js";

import express from "express";
import {
    filemanager,FilesMetadata,getFileContent,
} from "../Controllers/ResultController.js";

const router = express.Router({ mergeParams: true });

router.route("/")
  .post(authenticate, restrict(["doctor"]), filemanager)
  .get(getFileContent);
 router.get(FilesMetadata);
 router.get(getFileContent);

 

export default router;