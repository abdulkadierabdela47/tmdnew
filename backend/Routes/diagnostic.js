import express from "express";
import {
  registerDiagnostic,
  getSingleDiagnosticUser,
  getAllDiagnostic,
  updateDiagnostic,
  deleteDiagnostic,
  getDiagnosticUserProfile,
  getMyAppointmentsn,
  getDiagnosticTransaction,
  createCredit,
  addservices,
} from "../Controllers/DiagnosticController.js";

import { authenticate, restrict } from "../auth/verifyToken.js";

const router = express.Router();
router.post(
  "/register",
  authenticate,
  restrict(["admin", "finance"]),
  registerDiagnostic,
  
);
router.post(
  "/createCredit",
  authenticate,
  restrict(["admin", "finance"]),
  createCredit
);
router.post(
  "/addservices",
  authenticate,
  restrict(["admin", "finance"]),
  addservices
);




router.get("/:id", authenticate, getSingleDiagnosticUser);
router.get(
  "/",
  authenticate,
  restrict(["doctor", "admin", "finance"]),
  getAllDiagnostic
);

router.get(
  "/diagnostic/transactions",
  authenticate,
  restrict(["admin", "finance"]),
  getDiagnosticTransaction
);

router.put("/:id", authenticate, restrict(["doctor"]), updateDiagnostic);
router.delete("/:id", authenticate, restrict(["doctor"]), deleteDiagnostic);
router.get(
  "/profile/me",
  authenticate,
  restrict(["diagnostic"] && ["admin"]),
  getDiagnosticUserProfile
);
router.get(
  "/appointments/my-appointments",
  authenticate,

  getMyAppointmentsn
);




export default router;
