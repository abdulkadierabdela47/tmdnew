import { Timestamp } from "mongodb";
import mongoose from "mongoose";

const historySchema = new mongoose.Schema(
  {
    doctor: {
      type: mongoose.Types.ObjectId,
      ref: "Doctor",
      required: true,
    },
    user: {
      type: mongoose.Types.ObjectId,
      ref: "User",
      required: true,
    },
    // booking: {
    //   type: mongoose.Types.ObjectId,
    //   ref: "Booking",
    //   required: true,
    // },
    // diagnostic_center: {
    //   type: mongoose.Types.ObjectId,
    //   ref: "Diagnostic_center",
    // },
    date: {
      type: String,
    },

    diagnosis: {
      type: String,
    },
    history: { type: String },
    progress_note: { type: String },
    treatment: { type: String },
    prescription: { type: String },

    status: {
      type: String,
      enum: ["pending", "completed ", "waiting", "in-person"],
      default: "pending",
    },
    isCompleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

historySchema.pre(/^find/, function (next) {
  this.populate("user").populate({
    path: "user",
    select: "name",
  });
  next();
});

export default mongoose.model("History", historySchema);
