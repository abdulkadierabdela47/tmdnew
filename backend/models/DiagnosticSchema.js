import mongoose from "mongoose";

const DiagnosticSchema = new mongoose.Schema(
  {
    companyName: { type: String, required: true, unique: true },
    city: { type: String },
    Woreda: { type: String },
    subCity: { type: String },
    phone: { type: Number },
    email: { type: String, required: true, unique: true },

    password: { type: String, required: true },
    username: { type: String },
    role: {
      type: String,

      default: "diagnostic",
    },
    isApproved: {
      type: String,
      enum: ["pending", "approved", "cancelled"],
      default: "approved",
    },
    photo: { type: String },
    credit: { type: Number },
    service: { type: Array },
    appointments: [{ type: mongoose.Types.ObjectId, ref: "Booking" }],
    doctor: {
      type: mongoose.Types.ObjectId,
      ref: "Doctor",
    },
    user: {
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);

export default mongoose.model("Diagnostic", DiagnosticSchema);
