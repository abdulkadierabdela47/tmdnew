import mongoose from "mongoose";

const fileSchema = new mongoose.Schema(
  {
    name: String,
    filePath: String, 
  },
);


const Result = mongoose.model("File", fileSchema);

export default Result;




