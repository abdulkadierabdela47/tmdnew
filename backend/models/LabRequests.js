import mongoose from 'mongoose';

const { Schema } = mongoose;

// Define LabRequest schema
const labRequestSchema = new Schema({
  tests: [{
    testname: {
      type: String,
      required: true
    },
    testprice: {
      type: Number,
      required: true
    }
  }],
  center: {
    type: Schema.Types.ObjectId,
    ref: 'DiagnosticCenter',
    required: true
  }
});

// Create LabRequest model
const LabRequest = mongoose.model('LabRequest', labRequestSchema);

export default LabRequest;
