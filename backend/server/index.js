const bcrypt = require("bcryptjs");
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const DiagnosticCenterModel = require("./model/Diagnoscenter");
const PatientDataModel =require("./model/Patient");
const PaymentModel = require('./model/Payment');
const ResultModel = require("./model/Result");
const DoctorModel = require("./model/doctor");
const multer  = require("multer");
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');
const path = require('path');
const crypto = require("crypto");
const fs = require("fs");

require("../server/model/pdfDetails");

const app = express();
app.use(express.json());
app.use(cors());
app.use("/Med", express.static("Med"));

mongoose.connect("mongodb://localhost:27017/TeleMed-backend");

app.post("/login", (req, res) => {
    const { email, password } = req.body;
    DiagnosticCenterModel.findOne({ email: email })
        .then(user => {
            if (user) {
                bcrypt.compare(password, user.password, (err, result) => {
                    if (result) {
                        res.json("Success");
                    } else {
                        res.json("The password is incorrect");
                    }
                });
            } else {
                res.json("No record existed");
            }
        });
});


app.put("/update", async (req, res) => {
    try {
      const { id, centername, address } = req.body;
  
      // Check if the user exists
      const user = await DiagnosticCenterModel.findOne({ _id: id });
      if (!user) {
        return res.status(404).json({ error: "User not found" });
      }
  
      // Update the user information
      user.centername = centername;
      user.address = address;
    
  
      await user.save();
  
      res.json(user);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal Server Error" });
    }
  });
  
app.post("/register", async (req, res) => {
    try {
        const { centername, address, email, password } = req.body;
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(password, salt);
        const diagnosticCenter = await DiagnosticCenterModel.create({
            centername,
            email,
            address,
            password: hashedPassword,
        });
        res.json(diagnosticCenter);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Internal Server Error" });
    }
});

app.get('/centers', async (req, res) => {
    try {
        
        const diagnosticCenters = await DiagnosticCenterModel.find();
        res.json(diagnosticCenters);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.get('/getpatient', async (req, res) => {
  try {
    const {  userId } = req.query;
 
      const PatientData = await PatientDataModel.find({diagnostic_center_id: userId});
      //console.log(PatientData);
      res.json(PatientData);

  } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
  }
});



app.get('/userdetail', async (req, res) => {
    try {
      const email = req.query.email; 
      const user = await DiagnosticCenterModel.findOne({ email }); 
  
      if (!user) {
        return res.status(404).json({ error: 'User not found' });
      }
  
      res.json(user);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });
  
  app.get('/searchbyname', async (req, res) => {
    const { firstName, userId } = req.query;
    try {
      // Find users with matching first name and diagnostic center ID equal to user ID
      const users = await PatientDataModel.find({ first_name: firstName,  diagnostic_center_id: userId });
  
      res.json(users);
      console.log(users);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Error fetching data' });
    }
  });



    
  app.get('/searchbyphone', async (req, res) => {
    const { phone_number, userId } = req.query;
    try {
      const users = await PatientDataModel.find({ phone_number: phone_number, diagnostic_center_id: userId });
  
      res.json(users);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Error fetching data' });
    }
  });

  






// result uploading 


  const storage = multer.diskStorage({
  destination: function (req, file, cb) {
  cb(null, "./Med");
    },
    filename: function (req, file, cb) {
     const uniqueSuffix = Date.now();
    cb(null, uniqueSuffix + file.originalname);
    },
  });
  
   const PdfSchema = mongoose.model("PdfDetails");
  const upload = multer({ storage: storage });
  
  app.post("/upload-files", upload.single("file"), async (req, res) => {
    console.log(req.file);
    const title = req.body.title;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const fileName = req.file.filename;
    const patient_id = req.body.patient_id; // Use req.body.patient_id to access the patient ID

    const diagnosticCenterId = req.body.diagnostic_center_id; // Get diagnostic center ID from request body
    const currentDate = new Date().toLocaleDateString('en-US', { month: '2-digit' ,day: '2-digit', year: 'numeric' }); // Get current date in the format: month/day/year

    try {
      await PdfSchema.create({
        title: title,
        first_name:firstname,
        last_name:lastname,
        patient_id:patient_id,
        pdf: fileName,
        diagnostic_center_id: diagnosticCenterId, // Store diagnostic center ID in MongoDB
        date: currentDate // Register current date in the MongoDB collection
      });
      res.send({ status: "ok" });
    } catch (error) {
      res.json({ status: error });
    }
});


//for diagnostic centersto there results
app.get("/get-files/:diagnosticCenterId", async (req, res) => {
  try {
    const diagnosticCenterId = req.params.diagnosticCenterId;
    let query = { patient_id: diagnosticCenterId };   

    PdfSchema.find(query).then((data) => {
      res.send({ status: "ok", data: data });
    });
  } catch (error) {
    console.error(error);
    res.status(500).send({ status: "error", message: "Internal server error" });
  }
});


//for the doctor to see patient result





app.get("/get-results/:patient_id", async (req, res) => {
  try {
    const patient_id = req.params.patient_id;
    let query = { patient_id: patient_id };   

    PdfSchema.find(query).then((data) => {
      // Map the data to include the required properties
      const mappedData = data.map(item => ({
        title: item.title,
        first_name: item.first_name,
        last_name: item.last_name,
        date: item.date,
        pdf: item.pdf
      }));

      res.send({ status: "ok", data: mappedData });
    });
  } catch (error) {
    console.error(error);
    res.status(500).send({ status: "error", message: "Internal server error" });
  }
}); 


  
app.post('/payment', async (req, res) => {
  try { 
    const { first_name, last_name, total_price, PatientId, diagnostic_center_id, status, date } = req.body;
    
    const payment = await PaymentModel.create({
      PatientId,
      first_name,
      last_name,
      diagnostic_center_id, 
      total_price,
      date, 
      status,
    });
  
    res.json(payment);
  } catch(error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});
  
    app.put("/update-balance", async (req, res) => {
      try {
        const { email, credit } = req.body;
    
        // Find the diagnostic center by email
        const center = await DiagnosticCenterModel.findOne({ email });
    
        // Check if the diagnostic center exists
        if (!center) {
          return res.status(404).json({ error: "Diagnostic center not found" });
        }
    
        // Update the balance
        center.credit = credit;
    
        // Save the updated diagnostic center
        await center.save();
    
        // Send response
        res.json({ message: "Balance updated successfully", updatedCenter: center });
      } catch (error) {
        console.error("Error updating balance:", error);
        res.status(500).json({ error: "Internal Server Error" });
      }
    });
    



   // app.get('/finddoctorid', async (req, res) => {
    //  try {
          
      //    const doctor = await DoctorModel.find();
      //    res.json(doctor);
      //    console.log(res);
      //} catch (error) {
        //  console.error(error);
          // res.status(500).json({ error: 'Internal Server Error' });
      // }
   // });
  

// server.js




// Initialize express app

// Set up MongoDB connection
 /**    mongoose.connect("mongodb://localhost:27017/TeleMed-backend");
const FileSchema = new mongoose.Schema({
  filename: String,
  contentType: String,
  data: Buffer
});
// Initialize GridFS stream
let gfs;

mongoose.connection.once("open", () => {
  // Initialize GridFS stream with mongoose connection
  gfs = Grid(mongoose.connection.db, mongoose.mongo);
  gfs.collection("uploads");
});

// Create storage engine using multer
const storage = multer.diskStorage({
  destination: "uploads/",
  filename: function (req, file, cb) {
    crypto.randomBytes(16, (err, buf) => {
      if (err) {
        return cb(err);
      }
      cb(null, buf.toString("hex") + path.extname(file.originalname));
    });
  },
});

// Initialize multer upload
const upload = multer({ storage });

// Upload endpoint
app.post("/upload", upload.single("file"), (req, res) => {
  res.json({ file: req.file });
});

// Download endpoint
app.get("/download/:filename", (req, res) => {
  gfs.files.findOne({ filename: req.params.filename }, (err, file) => {
    if (!file || file.length === 0) {
      return res.status(404).json({ error: "File not found" });
    }

    // Create read stream from GridFS
    const readstream = gfs.createReadStream(file.filename);
    readstream.pipe(res);
  });
});


 */


  app.get("/", async (req, res) => {
    res.send("Success!!!!!!");
  });



app.listen(3001, () => {
    console.log("Server is running");
});





