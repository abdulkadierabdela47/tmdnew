import express from "express";
import cookieParser from "cookie-parser";
import cors from "cors";
import mongoose from "mongoose";
import dontenv from "dotenv";
import multer from "multer";
import userRoute from "./Routes/user.js";
import authRoute from "./Routes/auth.js";
import doctorRoute from "./Routes/doctor.js";
import reviewRoute from "./Routes/review.js";
import bookingRoute from "./Routes/booking.js";
import historyRoute from "./Routes/history.js";
import diagnosticRoute from "./Routes/diagnostic.js";
import centralDataRoute from "./Routes/Lab.js";
import ResultRoute from  "./Routes/Result.js";
import DiagnosticCenterLogin from "./Routes/CenterLogin.js";

dontenv.config();

const app = express();
const port = process.env.PORT || 5000;

const coreOption = {
  origin: true,
};

app.get("/", (req, res) => {
  res.send("API is working");
});
//database
// mongoose
//   .connect(process.env.MONGO_URL, {})
//   .then((result) => console.log(`db connected${result}`))
//   .catch((err) => console.log(err));

mongoose.set("strictQuery", false);
const connectDB = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URL);
    console.log("mongo db connected ..");
  } catch (error) {
    console.log("db is not connected " + error);
  }
};
// middleware
app.use(express.json());

app.use(cookieParser());
app.use(cors(coreOption));
app.use("/api/v1/auth", authRoute); ///domain /api/v1/auth/registe or login
app.use("/api/v1/users", userRoute); ///domain /api/v1/auth/user update delete get
app.use("/api/v1/doctors", doctorRoute); ///domain /api/v1/user update delete get
app.use("/api/v1/reviews", reviewRoute); /// /api/v1/reviews  get create historyRoute
app.use("/api/v1/bookings", bookingRoute);
app.use("/api/v1/history", historyRoute);
app.use("/api/v1/diagnostic", diagnosticRoute);
app.use("/api/v1/Centraldata",centralDataRoute);
app.use("/api/v1/Result",ResultRoute);
app.use("/api/v1/diagnosticCenterLogin",DiagnosticCenterLogin);


app.listen(port, () => {
  connectDB();
  console.log("Server is running " + port);
});
