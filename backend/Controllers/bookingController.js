import User from "../models/UserSchema.js";
import Doctor from "../models/DoctorSchema.js";
import Booking from "../models/BookingSchema.js";

export const getCheckoutSession = async (req, res) => {
  if (!req.body.doctor) req.body.doctor = req.params.doctorId;
  if (!req.body.user) req.body.user = req.userId;
  const { time, date, doctor, user, ticketPrice } = req.body;

  const newBook = new Booking({ time, date, ticketPrice, doctor, user });

  try {
    const savedBooked = await newBook.save();
    await Doctor.findByIdAndUpdate(doctor, {
      $push: { appointments: savedBooked._id },
    });
    await User.findByIdAndUpdate(user, {
      $push: { appointments: savedBooked._id },
    });
    res.status(200).json({ success: true, message: "Successfully Booked" });
  } catch (error) {
    console.log(error);
    res
      .status(500)
      .json({ success: false, message: "Error Booking" + req.body });
  }
};
// create review

export const createReview = async (req, res) => {
  if (!req.body.doctor) req.body.doctor = req.params.doctorId;
  if (!req.body.user) req.body.user = req.userId;
  const { time, date, ticketPrice } = req.body;

  const newReview = new Review(time, date, ticketPrice);

  try {
    const savedReview = await newReview.save();
    await Doctor.findByIdAndUpdate(req.body.doctor, {
      $push: { reviews: savedReview._id },
    });
    res
      .status(200)
      .json({ success: true, message: "Review submitted", data: savedReview });
  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};

export const getAllDoctor = async (req, res) => {
  try {
    const status = req.query.status;
    const id = req.params.id;
    console.log(req.userId);
    // const doctorId = req.userId;
    // const status=req.body.status

    // const doctorId = "65deef870ff07ccd34862221";
    // const { query } = req.query;
    let doctors;
    // if (query) {
    //   doctors = await Booking.find({
    //     status: "approved",
    //     $or: [
    //       { name: { $regex: query, $options: "i" } },
    //       { specialization: { $regex: query, $options: "i" } },
    //     ],
    //   });
    // } else {
    //   doctors = await Booking.find({ status: "approved" });
    // }
    // const appointments = await Booking.find({ doctor: doctorId });
    // const appointments = await Booking.find({ doctor: doctorId });

    doctors = await Booking.find({ status: status, doctor: id });

    res.status(200).json({
      success: true,
      messate: "All Booked Found",
      data: doctors,
    });
  } catch (error) {
    res.status(404).json({ success: false, message: "Failed to find Booked" });
  }
};
