import Diagnostic from "../models/DiagnosticSchema.js";


export const LoginDiagnostic    = async (req, res) => {
    const { email, password } = req.body;
    Diagnostic.findOne({ email: email })
        .then(user => {
            if (user) {
                bcrypt.compare(password, user.password, (err, result) => {
                    if (result) {
                        res.json("Success");
                    } else {
                        res.json("The password is incorrect");
                    }
                });
            } else {
                res.json("No record existed");
            }
        });
};


