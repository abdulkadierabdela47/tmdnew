import multer from 'multer';
import fs from 'fs';
//import  Result  from "../models/fileSchema"; // Import the Result model

// Multer storage configuration for file upload
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'C:/uploadsTelemed'); // Save files to C:/uploadsTelemed folder
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

const upload = multer({ storage });

// Define the file manager function
export const filemanager = async (req, res) => {
  try {
    const uploadResult = await upload.single('file')(req, res); // Corrected typo in 'upload.single'
    if (!uploadResult) {
      return res.status(400).send('File upload failed');
    }

    const { originalname, path: filePath } = req.file;
    const newFile = new Result({
      name: originalname,
      filePath: filePath,
    });
    await newFile.save();
    res.status(200).send('File uploaded successfully');
  } catch (error) {
    console.error('Error uploading file:', error);
    res.status(500).send('Internal server error');
  }
};

// API endpoint for fetching file metadata
export const FilesMetadata = async (req, res) => {
  try {
    const files = await Result.find({}, 'name');
    res.json(files);
  } catch (error) {
    console.error('Error fetching files:', error);
    res.status(500).send('Internal server error');
  }
};

// API endpoint for fetching file content
export const getFileContent = async (req, res) => {
  try {
    const fileName = req.params.fileName;
    const file = await Result.findOne({ name: fileName });
    if (!file) {
      return res.status(404).send('File not found');
    }
    const filePath = file.filePath;
    const fileStream = fs.createReadStream(filePath);
    fileStream.pipe(res);
  } catch (error) {
    console.error('Error fetching file content:', error);
    res.status(500).send('Internal server error');
  }
};