import User from "../models/UserSchema.js";
import Booking from "../models/BookingSchema.js";
import Doctor from "../models/DoctorSchema.js";

export const updateUser = async (req, res) => {
  const id = req.params.id;

  try {
    const updateUser = await User.findByIdAndUpdate(
      id,
      { $set: req.body },
      { new: true }
    );

    res.status(200).json({
      success: true,
      messate: "successfully Updated",
      data: updateUser,
    });
  } catch (error) {
    res.status(500).json({ success: false, message: "Failed to update" });
  }
};

export const deleteUser = async (req, res) => {
  const id = req.params.id;

  try {
    await User.findByIdAndDelete(id);

    res.status(200).json({
      success: true,
      messate: "successfully deleted",
    });
  } catch (error) {
    res.status(500).json({ success: false, message: "Failed to delete" });
  }
};

export const getSingleUser = async (req, res) => {
  const id = req.params.id;

  try {
    const user = await User.findById(id).select("-password");

    res.status(200).json({
      success: true,
      messate: "User Found",
      data: user,
    });
  } catch (error) {
    res.status(404).json({ success: false, message: "Failed to find user" });
  }
};

export const getAllUser = async (req, res) => {
  //   const id = req.params.id;

  try {
    const users = await User.find({role:{$ne:"patient"}}).select("-password");

    res.status(200).json({
      success: true,
      messate: "All Users Found",
      data: users,
    });
    
  } catch (error) {
    res.status(404).json({ success: false, message: "Failed to find users" });
  }
};

export const getUserProfile = async (req, res) => {
  const userId = req.userId;

  try {
    const user = await User.findById(userId);
    if (!user) {
      return res
        .status(404)
        .json({ success: false, message: "User not found" });
    }

    const { password, ...rest } = user._doc;
    res.status(200).json({
      success: true,
      message: "profile info is getting",
      data: { ...rest },
    });
  } catch (error) {
    res.status(404).json({ success: false, message: "Something went wrong" });
  }
};

export const getMyAppointmentsn = async (req, res) => {
  const userId = req.userId;

  try {
    const user = await User.findById(userId);
    if (!user) {
      return res
        .status(404)
        .json({ success: false, message: "Doctor not found" });
    }
    //retrive appointments from booking schema
    const bookings = await Booking.find({ user: user, status: "pending" });

    // //extract doctor id
    // const doctorId = bookings.map((el) => el.doctor.id);

    // //retrieve docror using Dr. id
    // const doctors = await Doctor.find({ _id: { $in: doctorId } }).select(
    //   "-password"
    // );

    res.status(200).json({
      success: true,
      message: "Appointments are getting",
      data: { bookings },
    });
  } catch (error) {
    res.status(404).json({ success: false, message: "Something went wrong" });
  }
};

export const getAllMyAppointment = async (req, res) => {
  const userId = req.userId;

  try {
    const user = await User.findById(userId);
    if (!user) {
      return res
        .status(404)
        .json({ success: false, message: "Appointment not found" });
    }
    //retrive appointments from booking schema
    const bookings = await Booking.find({ user: user, status: "completed" });

    res.status(200).json({
      success: true,
      message: "My History Appointments",
      data: { bookings },
    });
  } catch (error) {
    res.status(404).json({ success: false, message: "Something went wrong" });
  }
};



